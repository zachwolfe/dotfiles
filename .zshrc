# The following lines were added by compinstall

zstyle ':completion:*' file-sort name
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' squeeze-slashes true
zstyle :compinstall filename '/home/zach/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install


bindkey "^[[7~" beginning-of-line
bindkey "^[[8~" end-of-line
bindkey ";5C" forward-word
bindkey ";5D" backward-word
