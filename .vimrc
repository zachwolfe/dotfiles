set smartindent
set number
set tabstop=2
set shiftwidth=2
set expandtab
" window
nmap <leader>sw<left>  :topleft  vnew<CR>
nmap <leader>sw<right> :botright vnew<CR>
nmap <leader>sw<up>    :topleft  new<CR>
nmap <leader>sw<down>  :botright new<CR>
" buffer
nmap <leader>s<left>   :leftabove  vnew<CR>
nmap <leader>s<right>  :rightbelow vnew<CR>
nmap <leader>s<up>     :leftabove  new<CR>
nmap <leader>s<down>   :rightbelow new<CR>

" reselect visual after indent
vnoremap < <gv
vnoremap > >gv

"update open files when changed externally
set autoread

"better split navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

"auto reload vimrc when changed
au BufWritePost .vimrc so ~/.vimrc

noremap <F1> <Esc> 

"insert blank lines without inserting
nmap t o<Esc>k
nmap T O<Esc>j
set pt=<f9>

colorscheme vydark 

set hlsearch " Search highlighting and stuff
set incsearch

set path=./*/**

set backspace=2 " make backspace actuallly work as backspace
syntax on
set directory=$HOME/.vimswap " swp and swo files are a pain in the ass to keep around in ./

" set ignorecase
let g:ctrl_p_working_path_mode = 0
au BufReadPost *.ejs set syntax=html

" indent-guides colors
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=black   ctermbg=0
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=grey ctermbg=8
